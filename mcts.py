import logging
import random

import numpy as np
from chess import Board

from chess_utils import POSSIBLE_MOVES, reward, move_lookup, MOVE_MAP, push_move, game_end_reason, board_state


class MCTS:
    def __init__(self, nn):
        self.visited = set()
        self.policies = {}
        self.action_visits = {}
        self.action_values = {}
        self.nn = nn

    def _search(self, board: Board, exploration):
        logging.debug("Searching:\n" + str(board))
        if board.is_game_over():
            logging.debug("Game end reached: " + game_end_reason(board))
            return -reward(board)

        state = board_state(board)
        if state not in self.visited:
            self.visited.add(state)
            self.policies[state], v = self.nn.evaluate(board)
            self.action_visits[state] = np.zeros(POSSIBLE_MOVES)
            self.action_values[state] = np.zeros(POSSIBLE_MOVES)
            return -v

        max_u, best_move = -float("inf"), None
        for move in board.legal_moves:
            movei = move_lookup(move.uci())
            u = self.action_values[state][movei] + exploration * self.policies[state][movei] * np.sqrt(
                1 + np.sum(self.action_visits[state])) / (1 + self.action_visits[state][movei])
            logging.debug("Move: " + move.uci() + " U: " + str(u))
            if u > max_u:
                max_u = u
                best_move = move
                logging.debug("Best?")
        logging.debug("Best move: " + best_move.uci())
        
        movei = move_lookup(best_move.uci())
        board.push(best_move)
        v = self._search(board, exploration)
        board.pop()
        logging.debug("Move: " + best_move.uci() + " V: " + str(v))
        
        self.action_values[state][movei] = (self.action_visits[state][movei] * self.action_values[state][movei] + v) / (
                self.action_visits[state][movei] + 1)
        self.action_visits[state][movei] += 1
        return -v

    def search_move(self, board, n_simulations, exploration):
        for s in range(n_simulations):
            logging.debug("Simulation: " + str(s+1) + "/" + str(n_simulations))
            self._search(board, exploration)
        state = board_state(board)
        policy = self.action_visits[state] / np.sum(self.action_visits[state])
        move = random.choices(range(len(policy)), weights=policy)[0]
        move = MOVE_MAP[move]
        return move, [state, policy, None]

    def best_move(self, board, n_simulations, exploration):
        for s in range(n_simulations):
            logging.debug("Simulation: " + str(s + 1) + "/" + str(n_simulations))
            self._search(board, exploration)
        state = board_state(board)
        policy = self.action_visits[state] / np.sum(self.action_visits[state])
        move = np.argmax(policy)
        move = MOVE_MAP[move]
        return move, [state, policy, None]