import logging
from os.path import isfile

import numpy as np

from chess_utils import send, POSSIBLE_MOVES, move_lookup, board_state
from engine import Engine
import sys
import argparse
import chess.pgn
from nn import NN
from train import self_train


def uci():
    engine = Engine()
    for line in sys.stdin:
        try:
            line = line.rstrip()
            logging.info("UCI command received: " + line)
            if line == "uci":
                send("uciok")
            if line == "ucinewgame":
                engine.reset()
            if line == "isready":
                send("readyok")
            if line.startswith("go"):
                best_move = engine.compute_best_move()
                send("bestmove " + best_move)
            if line.startswith("position"):
                words = line.split(" ")
                moves = words[3:]
                engine.move(moves)
            if line == "quit":
                logging.info("End")
                return
        except:
            logging.info("Unexpected error:" + str(sys.exc_info()))


def train():
    nn = NN()
    self_train(nn, 10, 3, 100, 1)


def train_pgn(filepath):
    prepared_path = filepath + "_prep"
    if not isfile(prepared_path):
        with open(filepath) as pgn:
            count = 0
            total = 0
            while True:
                offset = pgn.tell()
                headers = chess.pgn.read_headers(pgn)
                #print(game.headers)
                if headers is None:
                    break
                total += 1
                time_control = headers["TimeControl"]
                if time_control == "-":
                    base_time = 100000000
                else:
                    base_time = int(time_control.split("+")[0])
                min_elo = min(int(headers["BlackElo"]), int(headers["WhiteElo"]))
                if min_elo > 2100 and base_time >= 600:
                    pgn.seek(offset)
                    game = chess.pgn.read_game(pgn)
                    print(game, file=open(prepared_path, "a"), end="\n\n")
                    print("+", end="")
                    count += 1
                else:
                    print(".", end="")
                if total%200 == 0:
                    print("")
            print(str(count) + " games prepared.")
    with open(prepared_path) as pgn:
        nn = NN(False)
        game_count = 0
        while True:
            game = None
            examples = []
            i = 0
            while True:
                if i >= 100:
                    break
                game = chess.pgn.read_game(pgn)
                if game is None:
                    break
                if game.headers["Result"] == "1-0":
                    result = 1
                elif game.headers["Result"] == "0-1":
                    result = -1
                elif game.headers["Result"] == "1/2-1/2":
                    result = 0
                else:
                    print("unknown result")
                    print(str(game.headers["Result"]))
                    continue
                game_count += 1
                reward = result
                board = game.board()
                moves = []
                for move in game.mainline_moves():
                    moves.append(move)
                for mi, move in enumerate(moves):
                    state = board_state(board)
                    policy = np.zeros(POSSIBLE_MOVES)
                    m = move_lookup(move.uci())
                    policy[m] = 1
                    examples.append([state, policy, reward * (mi / len(moves))])
                    board.push(move)
                    reward = -reward
                i += 1
            nn.train(examples)
            nn.checkpoint()
            print("Game count: " + str(game_count))
            if game is None:
                break


def test():
    engine = Engine()
    engine.board.set_fen("1k6/ppp5/8/8/8/4R3/5PPP/6K1 w - - 0 1")
    print(engine.board)
    move = engine.compute_best_move()
    print("Best move: " + move)
    engine.board.push_uci(move)
    print(engine.board)
    assert engine.board.is_checkmate()
    print("OK!")


if __name__ == "__main__":
    logging.basicConfig(level=20)
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--uci', action='store_true')
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')
    parser.add_argument('--train_pgn')
    args = parser.parse_args()
    if args.uci:
        uci()
    elif args.train:
        train()
    elif args.test:
        test()
    elif args.train_pgn:
        train_pgn(args.train_pgn)
