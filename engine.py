import chess
import logging
from chess_utils import push_move
from mcts import MCTS
from nn import NN


class Engine:
    def __init__(self):
        logging.info("Engine starting...")
        self.nn = NN()
        self.board = chess.Board()
        self.mcts = MCTS(self.nn)
        logging.info("Engine started")

    def reset(self):
        self.board = chess.Board()
        self.mcts = MCTS(self.nn)

    def move(self, moves):
        self.board = chess.Board()
        for move in moves:
            push_move(self.board, move)

    def compute_best_move(self):
        move, _ = self.mcts.best_move(self.board, 100, 1)
        return move