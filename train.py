from chess import Board

from chess_utils import push_move, game_end_reason, reward
from mcts import MCTS


def self_train(nn, n_iter, n_episodes, n_sim, exploration):
    examples = []
    print("Training...")
    for i in range(n_iter):
        print("Iteration " + str(i + 1) + "/" + str(n_iter))
        for e in range(n_episodes):
            print("Episode " + str(e + 1) + "/" + str(n_episodes))
            examples += _executeEpisode(nn, n_sim, exploration)
        nn.train(examples)
        nn.checkpoint()
        # TODO play against previous version of NN to check progress


def _executeEpisode(nn, n_simulations, exploration):
    examples = []
    board = Board()
    mcts = MCTS(nn)

    while True:
        move, example = mcts.search_move(board, n_simulations, exploration)
        examples.append(example)
        push_move(board, move)
        print("")
        print(board)
        if board.is_game_over():
            print(game_end_reason(board))
            r = reward(board)
            for i in range(len(examples) - 1, -1, -1):
                r = -r
                examples[i][2] = r
            return examples
