import chess
from chess import Board
from tensorflow.keras import Model
from tensorflow.keras.models import load_model
from tensorflow.keras.layers import *
import numpy as np
from datetime import datetime
from os.path import isfile
from chess_utils import POSSIBLE_MOVES, repetitions, set_state
from tensorflow.python.keras.optimizer_v2.adam import Adam

WIDTH = 8
HEIGHT = 8
LAYERS = 20

CNN_FILTERS = 64
CNN_KERNEL_SIZE = 3

SE_CHANNELS = 10

TOWER_HEIGHT = 10


def se_block(x):
    y = AveragePooling2D(pool_size=(WIDTH, HEIGHT), data_format="channels_last")(x)
    y = Dense(SE_CHANNELS, activation="relu")(y)
    y = Dense(CNN_FILTERS, activation="sigmoid")(y)
    y = multiply([x, y])
    return y


def body_block(x):
    y = Conv2D(CNN_FILTERS, CNN_KERNEL_SIZE, use_bias=True, padding="same", data_format="channels_last")(x)
    y = Conv2D(CNN_FILTERS, CNN_KERNEL_SIZE, use_bias=True, padding="same", data_format="channels_last")(y)
    y = se_block(y)
    y = ReLU()(y)
    return y


def policy_head(x):
    y = Conv2D(CNN_FILTERS, CNN_KERNEL_SIZE, use_bias=True, padding="same", data_format="channels_last")(x)
    y = Conv2D(CNN_FILTERS, 8, use_bias=True, padding="valid", data_format="channels_last")(y)
    y = ReLU()(y)
    y = Dense(POSSIBLE_MOVES, activation="softmax")(y)
    y = Reshape((POSSIBLE_MOVES,))(y)
    return y


def value_head(x):
    y = Conv2D(CNN_FILTERS, CNN_KERNEL_SIZE, use_bias=True, padding="same", data_format="channels_last")(x)
    y = Conv2D(CNN_FILTERS, 8, use_bias=True, padding="valid", data_format="channels_last")(y)
    y = ReLU()(y)
    y = Dense(3, activation="softmax")(y)
    y = Reshape((3,))(y)
    return y


def get_board_state_for_piece(board, piece, color):
    return np.array(board.pieces(piece, color).tolist(), dtype=int).reshape((WIDTH, HEIGHT))


def compute_input(board: Board):
    layers = [get_board_state_for_piece(board, chess.PAWN, chess.WHITE),
              get_board_state_for_piece(board, chess.KNIGHT, chess.WHITE),
              get_board_state_for_piece(board, chess.BISHOP, chess.WHITE),
              get_board_state_for_piece(board, chess.ROOK, chess.WHITE),
              get_board_state_for_piece(board, chess.QUEEN, chess.WHITE),
              get_board_state_for_piece(board, chess.KING, chess.WHITE),
              get_board_state_for_piece(board, chess.PAWN, chess.BLACK),
              get_board_state_for_piece(board, chess.KNIGHT, chess.BLACK),
              get_board_state_for_piece(board, chess.BISHOP, chess.BLACK),
              get_board_state_for_piece(board, chess.ROOK, chess.BLACK),
              get_board_state_for_piece(board, chess.QUEEN, chess.BLACK),
              get_board_state_for_piece(board, chess.KING, chess.BLACK),
              np.full((WIDTH, HEIGHT), int(board.turn)),
              np.full((WIDTH, HEIGHT), int(board.has_kingside_castling_rights(chess.WHITE))),
              np.full((WIDTH, HEIGHT), int(board.has_queenside_castling_rights(chess.WHITE))),
              np.full((WIDTH, HEIGHT), int(board.has_kingside_castling_rights(chess.BLACK))),
              np.full((WIDTH, HEIGHT), int(board.has_queenside_castling_rights(chess.BLACK))),
              np.full((WIDTH, HEIGHT), board.halfmove_clock),
              np.full((WIDTH, HEIGHT), board.fullmove_number),
              np.full((WIDTH, HEIGHT), int(repetitions(board)))]
    return np.stack(layers, axis=2)


class NN:
    def __init__(self, reload=True):
        latest = None
        if reload and isfile("latest.txt"):
            with open("latest.txt", "r") as f:
                latest = f.readline()
        if latest:
            self.model = load_model(latest)
            self.model.compile(loss=['categorical_crossentropy', 'mean_squared_error'], optimizer=Adam())
            print("Loaded " + latest)
        else:
            input = Input(shape=(WIDTH, HEIGHT, LAYERS))
            y = input
            for i in range(TOWER_HEIGHT):
                y = body_block(y)
            policy = policy_head(y)
            value = value_head(y)
            self.model = Model(inputs=input, outputs=[policy, value])
            self.model.compile(loss=['categorical_crossentropy', 'mean_squared_error'], optimizer=Adam())
            self.model.summary()

    def evaluate(self, board: Board):
        input = compute_input(board)
        p, v = self.model(input.reshape((1, WIDTH, HEIGHT, LAYERS)))
        v = v.numpy().flatten()
        value = (v[0] - v[2]) / (v[0] + v[1] + v[2])
        return p.numpy().flatten(), value

    def train(self, examples):
        states = []
        policies = []
        values = []
        for example in examples:
            board = chess.Board()
            set_state(board, example[0])
            states.append(compute_input(board).reshape((WIDTH, HEIGHT, LAYERS)))
            policies.append(example[1])
            value = example[2]
            if value > 0:
                if value > 1:
                    value = 1
                values.append([value, 1 - value, 0])
            else:
                value = - value
                if value > 1:
                    value = 1
                values.append([0, 1 - value, value])
        states = np.stack(states)
        policies = np.stack(policies)
        values = np.stack(values)
        self.model.fit(states, [policies, values])

    def checkpoint(self):
        file = "models/" + datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
        self.model.save(file, include_optimizer=False)
        with open("latest.txt", "w") as f:
            f.write(file)
        print("Checkpoint: " + file)
