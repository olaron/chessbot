import logging

from chess import Board, Move


def chess_coord(x, y):
    if x < 0 or x > 7 or y < 0 or y > 7:
        return None
    return chr(ord('a') + x) + str(y + 1)


def line_moves_from(x, y, dx, dy):
    moves = []
    for i in range(1, 8):
        coord = chess_coord(x + dx * i, y + dy * i)
        if coord:
            moves.append(coord)
    return moves


def queen_moves_from(x, y):
    return line_moves_from(x, y, 1, -1) + \
           line_moves_from(x, y, 1, 0) + \
           line_moves_from(x, y, 1, 1) + \
           line_moves_from(x, y, 0, -1) + \
           line_moves_from(x, y, 0, 1) + \
           line_moves_from(x, y, -1, -1) + \
           line_moves_from(x, y, -1, 0) + \
           line_moves_from(x, y, -1, 1)


def knight_moves_from(x, y):
    moves = [chess_coord(x + 1, y + 2), chess_coord(x + 1, y - 2), chess_coord(x + 2, y + 1), chess_coord(x + 2, y - 1),
             chess_coord(x - 1, y + 2), chess_coord(x - 1, y - 2), chess_coord(x - 2, y + 1), chess_coord(x - 2, y - 1)]
    return [x for x in moves if x is not None]


def pawn_weak_promotions_from(x, y):
    promotions = []
    if y == 6:
        moves = [chess_coord(x - 1, 7), chess_coord(x, 7), chess_coord(x + 1, 7)]
        for move in moves:
            if move:
                promotions = promotions + [move + 'r', move + 'b', move + 'n']
    if y == 1:
        moves = [chess_coord(x - 1, 0), chess_coord(x, 0), chess_coord(x + 1, 0)]
        for move in moves:
            if move:
                promotions = promotions + [move + 'r', move + 'b', move + 'n']
    return promotions


def move_map():
    move_map = []
    for x in range(8):
        for y in range(8):
            start_coord = chess_coord(x, y)
            move_xy = queen_moves_from(x, y) + knight_moves_from(x, y) + pawn_weak_promotions_from(x, y)
            move_map = move_map + [start_coord + move for move in move_xy if move is not None]
    return move_map


MOVE_MAP = move_map()
POSSIBLE_MOVES = len(MOVE_MAP)
MOVE_LOOKUP = {v: i for i, v in enumerate(MOVE_MAP)}


def move_lookup(move: str):
    if move.endswith('q'):
        move = move[:-1]
    return MOVE_LOOKUP[move]


def game_end_reason(board: Board):
    if board.is_game_over():
        turn = board.turn
        turn = "White" if turn else "Black"
        if board.is_checkmate():
            return str(turn) + ": checkmated"
        if board.is_repetition():
            return str(turn) + ": 3 repetitions"
        if board.is_insufficient_material():
            return str(turn) + ": insufficient material"
        if board.is_seventyfive_moves():
            return str(turn) + ": 75 moves"
        if board.is_stalemate():
            return str(turn) + ": stalemate"
        return str(turn) + ": unknown"
    else:
        return "not end"


def push_move(board: Board, move: str):
    m = Move.from_uci(move)
    if board.is_legal(m):
        board.push(m)
    else:
        board.push_uci(move + 'q')


def reward(board):
    if board.is_checkmate():
        return -1
    else:
        return 0


def send(string):
    logging.info("UCI sent: " + string)
    print(string, flush=True)


def board_state(board: Board) -> tuple:
    return tuple(board.move_stack)


def set_state(board: Board, state: tuple):
    board.reset()
    for move in state:
        board.push(move)


def repetitions(board: Board, max: int = 3) -> int:
    transposition_key = board._transposition_key()
    switchyard = []
    count = 1
    try:
        while True:
            if count >= max:
                break

            if len(board.move_stack) < 1:
                break

            move = board.pop()
            switchyard.append(move)

            if board.is_irreversible(move):
                break

            if board._transposition_key() == transposition_key:
                count += 1
    finally:
        while switchyard:
            board.push(switchyard.pop())

    return count